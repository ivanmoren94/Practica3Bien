package torneo;

import static org.junit.Assert.*;

import org.junit.Test;

import junit.framework.TestCase;

public class PersonaTest extends TestCase {

	@Test
	public void test() {
		Persona persona1 = new Persona();
		persona1.id = 1;
		persona1.nombre = "victor";
		persona1.Apellidos = "valverde";
		persona1.equipo_id = 1;
		persona1.direccion = "no se";
		persona1.cp = 30000;
		persona1.Pais = "Mongolia";
		persona1.fechaNacimiento = "01/01/2000";
		persona1.ciudad = "Lorca";
		persona1.provincia = "Murcia";
		
		Persona persona2 = new Persona();
		persona2.id = 2;
		persona2.nombre = "victor";
		persona2.Apellidos = "valverde";
		persona2.equipo_id = 1;
		persona2.direccion = "no se";
		persona2.cp = 30000;
		persona2.Pais = "Mongolia";
		persona2.fechaNacimiento = "01/01/2000";
		persona2.ciudad = "Lorca";
		persona2.provincia = "Murcia";
		
		Persona persona3 = new Persona();
		persona3.id = 3;
		persona3.nombre = "victor";
		persona3.Apellidos = "valverde";
		persona3.equipo_id = 2;
		persona3.direccion = "no se";
		persona3.cp = 30000;
		persona3.Pais = "Mongolia";
		persona3.fechaNacimiento = "01/01/2000";
		persona3.ciudad = "Lorca";
		persona3.provincia = "Murcia";
		
		Persona persona4 = new Persona();
		persona4.id = 4;
		persona4.nombre = "victor";
		persona4.Apellidos = "valverde";
		persona4.equipo_id = 2;
		persona4.direccion = "no se";
		persona4.cp = 30000;
		persona4.Pais = "Mongolia";
		persona4.fechaNacimiento = "01/01/2000";
		persona4.ciudad = "Lorca";
		persona4.provincia = "Murcia";
		
		Persona persona5 = new Persona();
		persona5.id = 5;
		persona5.nombre = "victor";
		persona5.Apellidos = "valverde";
		persona5.equipo_id = 3;
		persona5.direccion = "no se";
		persona5.cp = 30000;
		persona5.Pais = "Mongolia";
		persona5.fechaNacimiento = "01/01/2000";
		persona5.ciudad = "Lorca";
		persona5.provincia = "Murcia";
		
		Persona persona6 = new Persona();
		persona6.id = 6;
		persona6.nombre = "victor";
		persona6.Apellidos = "valverde";
		persona6.equipo_id = 3;
		persona6.direccion = "no se";
		persona6.cp = 30000;
		persona6.Pais = "Mongolia";
		persona6.fechaNacimiento = "01/01/2000";
		persona6.ciudad = "Lorca";
		persona6.provincia = "Murcia";
		
		Persona persona7 = new Persona();
		persona7.id = 7;
		persona7.nombre = "victor";
		persona7.Apellidos = "valverde";
		persona7.equipo_id = 4;
		persona7.direccion = "no se";
		persona7.cp = 30000;
		persona7.Pais = "Mongolia";
		persona7.fechaNacimiento = "01/01/2000";
		persona7.ciudad = "Lorca";
		persona7.provincia = "Murcia";
		
		Persona persona8 = new Persona();
		persona8.id = 8;
		persona8.nombre = "victor";
		persona8.Apellidos = "valverde";
		persona8.equipo_id = 4;
		persona8.direccion = "no se";
		persona8.cp = 30000;
		persona8.Pais = "Mongolia";
		persona8.fechaNacimiento = "01/01/2000";
		persona8.ciudad = "Lorca";
		persona8.provincia = "Murcia";
	}

}
